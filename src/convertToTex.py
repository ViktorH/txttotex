'''
Add tex annotations to a .txt document,
\documentclass,\begin,\end,\section,... 
'''
import re

# Adds Latex beginning and end tag.
def addFormat(txtfile):
    #txt = txtfile.read()
    begin = re.sub(r"^", r"\\documentclass{article}\n\\begin{document}\n", txtfile)
    end = re.sub(r"$", r"\n\\end{document}", begin)
    return end

# Adds Sections
def addSection(txtfile):
    sect = re.sub(r"\d+.\s(.*)", r"\n\t\\section{\1}", txtfile)
    return sect

#NOT CURRENTLY IN USE
def addTitle(txtfile):
    title = re.sub(r"pattern", r"repl", txtfile)
    return title

def main(txt, tex):
    txt = open(txt, "r+", encoding="utf-8")
    newTxt = txt.read()
    print(newTxt + "\n\n")

    newTxt = addFormat(newTxt) #add latex format
    newTxt = addSection(newTxt) #add headlines as sections
    txt.close()

    tex = open(tex, "r+", encoding="utf-8")
    tex.write(newTxt)
    tex.close()
    print(newTxt)

if __name__ == '__main__':
    main("C:/Users/Viktor/Documents/Code/txttotex/txtFiles/doctest.txt", "C:/Users/Viktor/Documents/Code/txttotex/latexFiles/testTex.tex")

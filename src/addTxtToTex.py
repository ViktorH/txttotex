'''
Finn matchende overskrifter, kopier følgene tekst fra txt til tex filen.
Suggestion: bruke latex i googledoc teksten. 
re.sub tex file; Match tex sections by (text)list number.
'''
#s"\\section{(.*?)}\n(.*?)\\section|\\sub"
#\\section{.*?}
#section{(.+?)}\n(.+?)\\
import re
#List all text under every section.
#____________________________________________________________
txt = "txtFile.txt"
txt = open(txt, "r+", encoding="utf-8")

tx = txt.read()
sectionText = re.findall(r"section{.+?}\n(.+?)\\", tx, re.DOTALL)

#print(sectionText)
txt.close()

# Substitute every section with matching list nr text. 
#____________________________________________________________
tex = "texFile.tex"
tex = open(tex,"r+", encoding="utf-8")
te = tex.read()
tex.close()
txtSection = re.findall(r"section({.+?})", te)

#Remove all pre-existing text. 
removeText = re.compile(r"section({.+?}\n).*?\\", re.DOTALL)
newTe = removeText.sub(r"section\1\\", te, re.DOTALL)
#newTe = re.sub(r"section({.+?}\n).*?\\", r"section\1\\", te, re.DOTALL)
print(newTe)

regHead = re.compile(txtSection[0])
n = 0
#newTe = te
#For every matching section name
#Replace with section Text.
for headline in sectionText:
    regHead = re.compile(txtSection[n])
    newTe = regHead.sub(txtSection[n] + "\n" + sectionText[n], newTe, re.DOTALL)
    n = n+1

#print(newTe)

#Since the file is read in line 25. It can't be replace before its reopened.
tex = "texFile.tex"
tex = open(tex,"r+", encoding="utf-8")
tex.write(newTe)
tex.close()
